FROM debian:jessie

# install java runtime
RUN set -ex && \
    echo 'deb http://deb.debian.org/debian jessie-backports main' \
      > /etc/apt/sources.list.d/jessie-backports.list && \
    apt update -y && \
    apt install -t \
      jessie-backports \
      openjdk-8-jre-headless \
      ca-certificates-java -y

# install curl & mono & nuget
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF \
  && echo "deb http://download.mono-project.com/repo/debian stable-jessie main" | tee /etc/apt/sources.list.d/mono-official-stable.list \
  && apt-get update \
  && apt-get -y install curl mono-complete \
  && cd /usr/bin \
  && curl -O https://dist.nuget.org/win-x86-commandline/v4.6.1/nuget.exe \
  && echo 'mono /usr/bin/nuget.exe  "$@"' > nuget \
  && chmod +x nuget

# install sonar scanner for msbuild
RUN apt-get -y install unzip \
  && cd /usr/bin \
  && curl -LJO https://github.com/SonarSource/sonar-scanner-msbuild/releases/download/4.3.1.1372/sonar-scanner-msbuild-4.3.1.1372-netcoreapp2.0.zip \
  && unzip sonar-scanner-msbuild-4.3.1.1372-netcoreapp2.0.zip \
  && rm sonar-scanner-msbuild-4.3.1.1372-netcoreapp2.0.zip \
  && echo 'dotnet /usr/bin/SonarScanner.MSBuild.dll  "$@"' > sonar-scanner-msbuild \
  && chmod +x sonar-scanner-msbuild \
  && chmod +x /usr/bin/sonar-scanner-3.2.0.1227/bin/sonar-scanner

# install .NET Core
RUN apt-get -y install curl libunwind8 gettext apt-transport-https \
  && curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg \
  && mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg \
  && sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-jessie-prod jessie main" > /etc/apt/sources.list.d/dotnetdev.list' \
  && apt-get update \
  && apt-get -y install dotnet-sdk-2.1.201 \
  && export PATH=$PATH:$HOME/dotnet

# install dependency-check
RUN cd /usr/bin \
  && curl -LJO http://dl.bintray.com/jeremy-long/owasp/dependency-check-3.3.0-release.zip \
  && unzip dependency-check-3.3.0-release.zip \
  && rm dependency-check-3.3.0-release.zip \
  && mv /usr/bin/dependency-check /usr/bin/depcheck \
  && echo '/usr/bin/depcheck/bin/dependency-check.sh "$@"' > dependency-check \
  && chmod +x dependency-check \
  && chmod +x /usr/bin/depcheck/bin/dependency-check.sh

# install docfx
RUN cd /usr/bin \
  && mkdir docfx \
  && cd docfx \
  && curl -LJO https://github.com/dotnet/docfx/releases/download/v2.37.1/docfx.zip \
  && unzip docfx.zip \
  && rm docfx.zip \
  && cd .. \
  && echo 'mono /usr/bin/docfx/docfx.exe "$@"' > doc-fx \
  && chmod +x /usr/bin/doc-fx

# update dependency-check definitions
RUN dependency-check --updateonly

# versions
RUN echo "+------+" \
  && echo "| JAVA |" \
  && echo "+------+" \
  && java -version \
  && echo "+------+" \
  && echo "| MONO |" \
  && echo "+------+" \
  && mono --version \
  && echo "+-------+" \
  && echo "| NUGET |" \
  && echo "+-------+" \
  && nuget help \
  && echo "+-------+" \
  && echo "| SONAR |" \
  && echo "+-------+" \
  && sonar-scanner-msbuild /? \
  && echo "+--------+" \
  && echo "| DOTNET |" \
  && echo "+--------+" \
  && dotnet --version \
  && echo "+------------------+" \
  && echo "| DEPENDENCY CHECK |" \
  && echo "+------------------+" \
  && dependency-check --version \
  && echo "+-------+" \
  && echo "| DOCFX |" \
  && echo "+-------+" \
  && doc-fx help

WORKDIR /