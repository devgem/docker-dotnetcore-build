# devgem/dotnetcore-build

[![java](https://img.shields.io/badge/java-1.8.0__171-brightgreen.svg)](http://openjdk.java.net/install/)
 [![mono](https://img.shields.io/badge/mono-5.12.0.301-brightgreen.svg)](http://www.mono-project.com/download/stable/#download-lin-debian)
 [![dotnet-sdk](https://img.shields.io/badge/dotnet--sdk-2.1.201-brightgreen.svg)](https://github.com/dotnet/sdk/releases)
 [![sonar-scanner](https://img.shields.io/badge/sonar--scanner-4.1.3-brightgreen.svg)](https://docs.sonarqube.org/display/SCAN/Scanning+on+Linux+or+macOS)
 [![nuget](https://img.shields.io/badge/nuget-4.6.1.5019-brightgreen.svg)](https://docs.microsoft.com/en-us/nuget/install-nuget-client-tools)
 [![dependency-check](https://img.shields.io/badge/dependency--check-3.3.0-brightgreen.svg)](https://jeremylong.github.io/DependencyCheck/dependency-check-cli/index.html)
 [![doc-fx](https://img.shields.io/badge/docfx-2.37.1.0-brightgreen.svg)](https://dotnet.github.io/docfx/)

[![Docker Stars](https://img.shields.io/docker/stars/devgem/dotnetcore-build.svg)](https://hub.docker.com/r/devgem/dotnetcore-build)
 [![Docker Pulls](https://img.shields.io/docker/pulls/devgem/dotnetcore-build.svg)](https://hub.docker.com/r/devgem/dotnetcore-build)
 [![Docker Layers](https://images.microbadger.com/badges/image/devgem/dotnetcore-build.svg)](https://microbadger.com/images/devgem/dotnetcore-build)
 [![Docker Version](https://images.microbadger.com/badges/version/devgem/dotnetcore-build.svg)](https://hub.docker.com/r/devgem/dotnetcore-build)

## Overview

This docker image starts from [debian:jessie](https://hub.docker.com/r/library/debian/tags/) and adds:

* openjdk-sdk
* mono
* sonar scanner for msbuild
* nuget
* dotnet-sdk
* dependency-check
* docfx

I use it in a [`Gitlab Runner`](https://docs.gitlab.com/runner/) to build, test and document [`.NET Core`](https://github.com/dotnet/core) projects.

To build the image:

```shell
docker build -t devgem/dotnetcore-build:VERSION .
```

## Versions

Pre-built images are available on Docker Hub:

| Image | Java | Mono | .NET Core | Sonar Scanner | Nuget | Dependency Check | DocFX | Remarks |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|
| [`devgem/dotnetcore-build:0.7`](https://hub.docker.com/r/devgem/dotnetcore-build/tags/) | 1.8.0_171 | 5.12.0.301 | 2.1.201 | 4.1.3| 4.6.1.5019 | 3.3.0 | 2.37.1.0 |Updated all|
| [`devgem/dotnetcore-build:0.6`](https://hub.docker.com/r/devgem/dotnetcore-build/tags/) | 1.8.0_131 | 5.10.1.4 | 2.1.104 | 4.1.1 | 4.6.1.5019 | 3.1.2 | 2.34.0.0 |Updated dependency-check database |
| [`devgem/dotnetcore-build:0.5`](https://hub.docker.com/r/devgem/dotnetcore-build/tags/) | 1.8.0_131 | 5.10.1.4 | 2.1.4 | 4.1.1 | 4.6.1.5019 | 3.1.1 | 2.33.1.0 |Updated dependency-check database |
| [`devgem/dotnetcore-build:0.4`](https://hub.docker.com/r/devgem/dotnetcore-build/tags/) | 1.8.0_131 | 5.10.0.160 | 2.1.4 | 4.0.2.892 | 4.6.0.4971 | 3.1.1 | 2.32.1.0 |Updated dependency-check database |
| [`devgem/dotnetcore-build:0.3`](https://hub.docker.com/r/devgem/dotnetcore-build/tags/) | 1.8.0_131 | 5.8.0.108 | 2.1.4 | 4.0.2.892 | 4.5.0.4696 | 3.1.1 | 2.31.0.0 |Updated dependency-check database |
| [`devgem/dotnetcore-build:0.2`](https://hub.docker.com/r/devgem/dotnetcore-build/tags/) | 1.8.0_131 | 5.8.0.108 | 2.1.4 | 4.0.2.892 | 4.5.0.4696 | 3.1.1 | N/A |Updated dependency-check database |
| [`devgem/dotnetcore-build:0.1`](https://hub.docker.com/r/devgem/dotnetcore-build/tags/) | 1.8.0_131 | 5.8.0.108 | 2.1.4 | 4.0.2.892 | 4.5.0.4696 | 3.1.1 | N/A |
